
$(document).ready(function () {

    // ======== Our Services Tabs ======== //

    $('.our-services-caption').click(function () {
        $(this)
            .addClass('active')
            .siblings()
            .removeClass('active')
            .closest('.tabs-container')
            .find('.our-services-content')
            .removeClass('active')
            .eq($(this)
                .index())
            .addClass("active");
    });
});


// ======== Our Amazing Work ======== //

    const buttonLoadMore = document.querySelector('.load-more-button-our-work');
    const tabsElements = document.querySelector('.our-work-tabs');
    const allPictures = document.getElementsByClassName('our-work-cell');
    const arrayBlock = Array.from(allPictures);
    const loader = document.querySelector('.loader');

    buttonLoadMore.addEventListener('click', function (event) {
            buttonLoadMore.classList.add('hidden');
            loader.classList.remove('hidden');

            setTimeout(() => {
                buttonLoadMore.classList.remove('hidden');
                loader.classList.add('hidden');
                arrayBlock.forEach(elem => {
                    elem.classList.remove('hidden')
                });
            }, 2000);
    });

    tabsElements.addEventListener('click', function (event) {
        tabsElements.querySelector('.active').classList.remove('active');
        event.target.classList.add('active');

        const data = event.target.dataset.type;
        arrayBlock.forEach(elem => {
            if (data === 'all') {
                elem.classList.remove('hidden');
            } else if (elem.dataset.type !== data) {
                elem.classList.add('hidden');
            } else {
                elem.classList.remove('hidden');
            }
        });
    });


// ====== CAROUSEL ====== //

const sliderContent = document.querySelectorAll('.slider-content');
const carousel = document.querySelectorAll('.feedback-image-block-small');
const previousBtn = document.querySelector('.previous');
const nextBtn = document.querySelector('.next');
let currentSlide = 0;

function showSlide(nextSlide) {
    sliderContent[currentSlide].classList.remove('active');
    sliderContent[nextSlide].classList.add('active');
    carousel[currentSlide].classList.remove('list-active');
    carousel[nextSlide].classList.add('list-active');
    currentSlide = nextSlide;
}

previousBtn.onclick = function() {
    const nextSlide = currentSlide === 0 ? sliderContent.length -1 : currentSlide -1;
    showSlide(nextSlide);

};

nextBtn.onclick = function() {
    const nextSlide = currentSlide === sliderContent.length -1 ? 0 : currentSlide+1;
    showSlide(nextSlide);

};

carousel.forEach((elem, index) => {
    elem.addEventListener('click', function(event) {
        showSlide(index);
    })
});



