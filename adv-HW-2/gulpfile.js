const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    cleaner = require('gulp-clean'),
    concat = require('gulp-concat'),
    minify = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    pipeline = require('readable-stream').pipeline,
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();

const path = {
    dist:{
        html:'dist',
        css:'dist/css',
        js:'dist/js',
        img : 'dist/img',
        ico: 'dist/favicon',
        self:'dist'
    },
    app : {
        html:'app/*.html',
        scss : 'app/scss/*.scss',
        js : 'app/js/*.js',
        img: 'app/img/*.*',
        ico: 'app/favicon/*.*',
    }
};
/**************** F U N C T I O N S ***************/
const htmlBuild = () => (
    gulp.app(path.app.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
);
const scssBuild = () => (
    gulp.app(path.app.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);
const imgBuild = () => (
    gulp.app(path.app.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);
const jsBuild = () => (
    gulp.app(path.app.js)
        .pipe(concat('script.js'))
        .pipe(minify())
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
);
const cleanProd = () =>( 
    gulp.app(path.dist.self, {allowEmpty: true})
        .pipe(cleaner())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(browserSync.stream())
);
const icoBuild = () => (
    gulp.app(path.app.ico)
        .pipe(gulp.dest(path.dist.ico))
        .pipe(browserSync.stream())
);
/****************** W A T C H E R ***************/
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch(path.app.html, htmlBuild).on('change',browserSync.reload);
    gulp.watch(path.app.scss, scssBuild).on('change',browserSync.reload);
    gulp.watch(path.app.js, jsBuild).on('change',browserSync.reload);
    gulp.watch(path.app.img, imgBuild).on('change',browserSync.reload);
};
/**************** T A S K S ****************/
gulp.task('default',gulp.series(
    cleanProd,
    htmlBuild,
    scssBuild,
    imgBuild,
    jsBuild,
    icoBuild,
    watcher,
));